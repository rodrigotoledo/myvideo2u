Rails.application.routes.draw do

  resources :videos, only: [:index, :new, :create, :edit, :update, :destroy]
  devise_scope :user do
    authenticated :user do
      root 'home#index', as: :authenticated_root
    end
  
    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  devise_for :users
end
