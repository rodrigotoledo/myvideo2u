require "rails_helper"

RSpec.feature "CRUD actions for logged user", type: :feature do
  describe '::List videos' do
    before do
      @user = create(:user)
      @another_user = create(:user)
      create_list(:video, 12, user_id: @user.id)
      create_list(:video, 20, user_id: @another_user.id)
      sign_in @user
      visit authenticated_root_path
    end

    scenario 'List only self videos' do
      main_menu.click_link 'VÍDEOS'
      
      expect(page).to have_selector('section table tbody tr', count: 12)
    end
  end

  describe '::Create video' do
    before do
      @user = create(:user)
      @video = create(:video, user_id: @user.id)
      @another_video = create(:video)
      sign_in @user
      visit authenticated_root_path
    end

    scenario 'Create self video' do
      main_menu.click_link 'ENVIAR VÍDEO'

      video_attributes = build(:video, user_id: @user.id)
      fill_in 'video_name', with: video_attributes.name
      fill_in 'video_url', with: "https://content.jwplatform.com/manifests/update_video.m3u8"
      click_on 'Salvar'

      expect(page).to have_content(video_attributes.name[0..10])
      expect(page).to have_content('Operação efetuada com sucesso!')
    end

    scenario 'Error on Create self video' do
      main_menu.click_link 'ENVIAR VÍDEO'

      video_attributes = build(:video, user_id: @user.id)
      fill_in 'video_name', with: ''
      fill_in 'video_url', with: ''
      click_on 'Salvar'

      expect(page).to have_content('Nome não pode ficar em branco')
      expect(page).to have_content('URL (Formato m3u8) não pode ficar em branco')
    end
  end

  describe '::Edit video' do
    before do
      @user = create(:user)
      @video = create(:video, user_id: @user.id)
      @another_video = create(:video)
      sign_in @user
      visit authenticated_root_path
    end

    scenario 'Edit self video' do
      main_menu.click_link 'VÍDEOS'

      find(:linkhref, edit_video_path(@video)).click

      video_attributes = build(:video)
      fill_in 'video_name', with: video_attributes.name
      fill_in 'video_url', with: "https://content.jwplatform.com/manifests/update_video.m3u8"
      click_on 'Salvar'

      expect(page).to have_content(video_attributes.name[0..10])
      expect(page).to have_content('Operação efetuada com sucesso!')
    end

    scenario 'Error on edit self video' do
      main_menu.click_link 'VÍDEOS'

      find(:linkhref, edit_video_path(@video)).click

      video_attributes = build(:video)
      fill_in 'video_name', with: ''
      fill_in 'video_url', with: ''
      click_on 'Salvar'

      expect(page).to have_content('Nome não pode ficar em branco')
      expect(page).to have_content('URL (Formato m3u8) não pode ficar em branco')
    end

    scenario 'Error on edit self video with invalid url' do
      main_menu.click_link 'VÍDEOS'

      find(:linkhref, edit_video_path(@video)).click

      video_attributes = build(:video)
      fill_in 'video_name', with: video_attributes.name
      fill_in 'video_url', with: 'abc'
      click_on 'Salvar'

      expect(page).to have_content('URL (Formato m3u8) inválido(a) ou não está no formato correto (.m3u8)')
    end

    scenario 'Error on Edit video to another author' do
      main_menu.click_link 'VÍDEOS'

      expect(page).not_to have_content(@another_video.name[0..10])
    end
  end


  describe '::Destroy video' do
    before do
      @user = create(:user)
      @video = create(:video, user_id: @user.id)
      sign_in @user
      visit authenticated_root_path
    end

    scenario 'Destroy self video' do
      main_menu.click_link 'VÍDEOS'

      find(:linkhref, video_path(@video)).click

      expect(page).to have_no_content(@video.name[0..10])
      expect(Video.find_by_id(@video.id)).to be_nil
      expect(page).to have_content('Operação efetuada com sucesso!')
    end
  end
end