# encoding: UTF-8
require "rails_helper"

RSpec.feature "Common user actions on system", type: :feature do
  describe '::Create user' do
    before do
      visit unauthenticated_root_path
      click_link 'Cadastre-se'
    end

    scenario 'Success on create user' do
      user_attributes = build(:user)
      fill_in 'user_email', with: user_attributes.email
      fill_in 'user_password', with: user_attributes.password
      fill_in 'user_password_confirmation', with: user_attributes.password
      click_on 'Clique aqui pra criar sua conta'

      expect(page).to have_content('Login efetuado com sucesso!')
    end

    scenario 'Error on create user without informations' do
      fill_in 'user_email', with: ''
      fill_in 'user_password', with: ''
      fill_in 'user_password_confirmation', with: ''
      click_on 'Clique aqui pra criar sua conta'

      expect(page).to have_content('Email não pode ficar em branco')
      expect(page).to have_content('Senha não pode ficar em branco')
    end

    scenario 'Error on create user with wrong email' do
      fill_in 'user_email', with: 'asd'
      fill_in 'user_password', with: 'aaaaaaaaaa'
      fill_in 'user_password_confirmation', with: 'aaaaaaaaaa'
      click_on 'Clique aqui pra criar sua conta'

      expect(page).to have_content('Email não é válido')
    end

    scenario 'Error on create user with wrong passwords' do
      user_attributes = build(:user)
      fill_in 'user_email', with: user_attributes.email
      fill_in 'user_password', with: '1'
      fill_in 'user_password_confirmation', with: '2'
      click_on 'Clique aqui pra criar sua conta'

      expect(page).to have_content('Senha é muito curto (mínimo: 6 caracteres)')
      expect(page).to have_content('Confirmação da senha não é igual a Senha')
    end
  end


  describe '::Edit user' do
    before do
      @user = create(:user)
      sign_in @user
      visit authenticated_root_path
      click_link 'EDITAR MEUS DADOS'
    end

    scenario 'Success on update user' do
      fill_in 'user_email', with: @user.email
      fill_in 'user_password', with: 'another_password'
      fill_in 'user_password_confirmation', with: 'another_password'
      fill_in 'user_current_password', with: @user.password
      click_on 'Clique aqui pra atualizar'

      expect(page).to have_content('Sua conta foi atualizada com sucesso')
    end

    scenario 'Error when pass wrong current password' do
      fill_in 'user_email', with: @user.email
      fill_in 'user_password', with: 'another_password'
      fill_in 'user_password_confirmation', with: 'another_password'
      fill_in 'user_current_password', with: 'fail_here'
      click_on 'Clique aqui pra atualizar'

      expect(page).to have_content('Senha atual (necessária para atualizar as informações) não é válido')
    end

    scenario 'Error when update user with wrong passwords' do
      fill_in 'user_email', with: @user.email
      fill_in 'user_password', with: '1'
      fill_in 'user_password_confirmation', with: '2'
      fill_in 'user_current_password', with: @user.password
      click_on 'Clique aqui pra atualizar'

      expect(page).to have_content('Senha é muito curto (mínimo: 6 caracteres)')
      expect(page).to have_content('Confirmação da senha não é igual a Senha')
    end
  end



  describe '::Forgot password' do
    before do
      visit unauthenticated_root_path
      click_link 'Esqueceu sua senha?'
    end

    scenario 'User exists on system and get email with success' do
      user = create(:user)
      fill_in 'user_email', with: user.email
      click_on 'Enviar instruções de acesso'

      expect(page).to have_content('Dentro de minutos, você receberá um e-mail com instruções para a troca da sua senha.')
    end

    scenario 'User doent exists on system' do
      fill_in 'user_email', with: 'anyuser@blablabla.com'
      click_on 'Enviar instruções de acesso'

      expect(page).to have_content('Email não encontrado')
    end
  end
end