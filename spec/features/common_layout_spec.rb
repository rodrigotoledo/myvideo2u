require "rails_helper"

RSpec.feature "Common layout features", type: :feature do
  describe '::Common access' do
    before do
      visit unauthenticated_root_path
    end

    scenario 'User access the home page and see common header and footer links' do
      expect(page).to have_link("ACESSAR SISTEMA")
      expect(page).to have_link("CADASTRE-SE")
      expect(page).to have_link("Publicidade")
      expect(page).to have_link("Negócios")
      expect(page).to have_link("Sobre")
      expect(page).to have_link("Privacidade")
      expect(page).to have_link("Termos")
    end
  end

  describe '::Without session check links' do
    before do
      visit unauthenticated_root_path
    end
    scenario "Show unlogged links" do
      expect(page).to have_link("ACESSAR SISTEMA")
      expect(page).to have_link("CADASTRE-SE")
    end
  end

  describe '::With Session check links' do
    before do
      user = create(:user)
      sign_in user
      visit authenticated_root_path
    end
    scenario "Show logged links" do
      expect(page).to have_link("ENVIAR VÍDEO")
      expect(page).to have_link("VÍDEOS")
      expect(page).to have_link("EDITAR MEUS DADOS")
      expect(page).to have_link("SAIR")
    end
  end

end