FactoryBot.define do
  
  factory :video do
    user
    name { Faker::Name.name }
    url { "https://content.jwplatform.com/manifests/yp34SRmf.m3u8" }
  end

  factory :user do
    email { Faker::Internet.email }
    password { 'admin123' }
    password_confirmation { 'admin123' }
  end
end