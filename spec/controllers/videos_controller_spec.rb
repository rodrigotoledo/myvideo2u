require 'rails_helper'

RSpec.describe VideosController, type: :controller do
  describe "DELETE destroy" do
    before do
      user = create(:user)
      @video = create(:video, user_id: user.id)
      @another_video = create(:video)
      sign_in user
    end

    it "success on destroy" do
      delete :destroy, params: { id: @video }
      expect(flash[:notice]).to match(/Operação efetuada com sucesso!/)
      expect(response.status).to eq(302)
    end

    it "error on destroy without permission" do
      delete :destroy, params: { id: @another_video }
      expect(flash[:notice]).to match(/Erro: Você não possui permissão a este conteúdo/)
      expect(response.status).to eq(302)
    end

    it "error on edit without permission" do
      get :edit, params: {id: @another_video}
      expect(flash[:notice]).to match(/Erro: Você não possui permissão a este conteúdo/)
      expect(response.status).to eq(302)
    end
  end
end
