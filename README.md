

## Before hands-on
Maybe you have some problems with the **pg** in MacOS, so maybe with the command you will get success

``gem install pg --no-doc  -- --with-pg-config=/Applications/Postgres.app/Contents/Versions/latest/bin/pg_config``

## Development environment with tests
``guard``


## Some links to help you in development

https://www.rubydoc.info/github/jnicklas/capybara/Capybara/RSpecMatchers

https://stackoverflow.com/questions/32628093/using-devise-in-rspec-feature-tests

https://about.futurelearn.com/blog/how-we-write-readable-feature-tests-with-rspec