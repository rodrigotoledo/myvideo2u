class Video < ApplicationRecord
  belongs_to :user
  validates :name, :url, presence: true

  validate :video_format
 
  def video_format
    if url.present? && !url.to_s.end_with?(".m3u8")
      errors.add(:url, I18n.t("errors.messages.not_equal_to_format"))
    end
  end
end
